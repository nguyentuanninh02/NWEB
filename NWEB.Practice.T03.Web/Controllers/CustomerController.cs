﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using NWEB.Practice.T03.Core.Entity;
using NWEB.Practice.T03.DataAccessLayer.IRepository;
using NWEB.Practice.T03.DataAccessLayer.Repository;

namespace NWEB.Practice.T03.Web.Controllers
{
    public class CustomerController : Controller
    {
        private readonly IUnitOfWork _unitOfWord;
        public CustomerController(IUnitOfWork categoryRepository)
        {
            _unitOfWord = categoryRepository;
        }
        public IActionResult Index(Guid? id)
        {
            List<Flower> flowers = null;
            if (id == null)
            {
                flowers = _unitOfWord.Flower.GetAll().ToList();
            }
            else
            {
                flowers = _unitOfWord.Flower.GetFlowerByCategory(id);
            }
            IEnumerable<SelectListItem> categoryLists = _unitOfWord.Category.GetAll().Select(u => new SelectListItem
            {
                Text = u.CategoryName,
                Value = u.Id.ToString(),
            }
            );
            ViewBag.categoryList = categoryLists;
            return View(flowers);
        }
        public IActionResult Search()
        {
            return View();
        }

        public IActionResult AddToCart()
        {
            TempData["success"] = "The flower is added to cart successfully";
            return RedirectToAction("Index");
        }
    }
}
