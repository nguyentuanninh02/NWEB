﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using NWEB.Practice.T03.Core;
using NWEB.Practice.T03.Core.Entity;
using NWEB.Practice.T03.DataAccessLayer.IRepository;
using System.Security.Claims;

namespace NWEB.Practice.T03.Web.Controllers
{
    [Authorize(Roles = SD.ROLE_ADMIN)]
    public class CategoryController : Controller
    {
        private readonly IUnitOfWork _unitOfWord;
        public CategoryController(IUnitOfWork categoryRepository)
        {
            _unitOfWord = categoryRepository;
        }

        public IActionResult Index()
        {
            List<Category> categories = _unitOfWord.Category.GetAll().ToList();
            return View(categories);
        }

        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Create(Category category)
        {
            if (ModelState.IsValid)
            {
                _unitOfWord.Category.Add(category);
                _unitOfWord.SaveChange();
                return RedirectToAction("Index");
            }
            return View(category);
        }

        public IActionResult Edit(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            Category? category = _unitOfWord.Category.Find(id);
            if (category == null)
            {
                return NotFound();
            }
            return View(category);
        }

        [HttpPost]
        public IActionResult Edit(Category category)
        {
            if (ModelState.IsValid)
            {
                _unitOfWord.Category.Update(category);
                _unitOfWord.SaveChange();
                return RedirectToAction("Index");
            }
            return View();
        }

        [HttpPost]
        public IActionResult Delete(Guid? id)
        {
            Category? category = _unitOfWord.Category.Find(id);
            if (category == null)
            {
                return NotFound();
            }
            _unitOfWord.Category.Delete(category);
            _unitOfWord.SaveChange();
            return RedirectToAction("Index");
        }
    }
}
