﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using NWEB.Practice.T03.Core;
using NWEB.Practice.T03.Core.Entity;
using NWEB.Practice.T03.DataAccessLayer.IRepository;
using NWEB.Practice.T03.DataAccessLayer.Repository;
using System.Security.Claims;

namespace NWEB.Practice.T03.Web.Controllers
{
    [Authorize(Roles = SD.ROLE_ADMIN)]
    public class FlowerController : Controller
    {
        private readonly IUnitOfWork _unitOfWord;
        public FlowerController(IUnitOfWork categoryRepository)
        {
            _unitOfWord = categoryRepository;
        }

        public IActionResult Index()
        {
            List<Flower> flowers = _unitOfWord.Flower.GetAllWithCategory().ToList();
            return View(flowers);
        }

        public IActionResult Create()
        {
            IEnumerable<SelectListItem> categories = _unitOfWord.Category.GetAll().Select(u => new SelectListItem
            {
                Text = u.CategoryName,
                Value = u.Id.ToString(),
            }
            );
            ViewBag.Category = categories;
            return View();
        }

        [HttpPost]
        public IActionResult Create(Flower flower)
        {
            if (ModelState.IsValid)
            {
                _unitOfWord.Flower.Add(flower);
                _unitOfWord.SaveChange();
                return RedirectToAction("Index");
            }
            IEnumerable<SelectListItem> categories = _unitOfWord.Category.GetAll().Select(u => new SelectListItem
                {
                    Text = u.CategoryName,
                    Value = u.Id.ToString(),
                }
            );
            ViewBag.Category = categories;
            return View(flower);
        }

        public IActionResult Edit(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            Flower? flower = _unitOfWord.Flower.Find(id);
            if (flower == null)
            {
                return NotFound();
            }
            IEnumerable<SelectListItem> categories = _unitOfWord.Category.GetAll().Select(u => new SelectListItem
            {
                Text = u.CategoryName,
                Value = u.Id.ToString(),
            }
);
            ViewBag.Category = categories;
            return View(flower);
        }

        [HttpPost]
        public IActionResult Edit(Flower flower)
        {
            if (ModelState.IsValid)
            {
                _unitOfWord.Flower.Update(flower);
                _unitOfWord.SaveChange();
                return RedirectToAction("Index");
            }
            return View();
        }

        [HttpPost, ActionName("Delete")]
        public IActionResult DeletePOST(Guid? id)
        {
            Flower? flower = _unitOfWord.Flower.Find(id);
            if (flower == null)
            {
                return NotFound();
            }
            _unitOfWord.Flower.Delete(flower);
            _unitOfWord.SaveChange();
            return RedirectToAction("Index");
        }
    }
}
