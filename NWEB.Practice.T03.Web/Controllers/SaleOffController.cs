﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using NWEB.Practice.T03.Core;
using NWEB.Practice.T03.Core.Entity;
using NWEB.Practice.T03.DataAccessLayer.IRepository;

namespace NWEB.Practice.T03.Web.Controllers
{
    [Authorize(Roles = SD.ROLE_ADMIN)]
    public class SaleOffController : Controller
    {
        private readonly IUnitOfWork _unitOfWord;
        public SaleOffController(IUnitOfWork categoryRepository)
        {
            _unitOfWord = categoryRepository;
        }
        public IActionResult Index()
        {
            IEnumerable<SelectListItem> categories = _unitOfWord.Category.GetAll().Select(u => new SelectListItem
            {
                Text = u.CategoryName,
                Value = u.Id.ToString(),
            }
            );
            ViewBag.Category = categories;
            return View();
        }
        [HttpPost]
        public IActionResult Index(Guid? flowerId, decimal saleOff)
        {
            Flower flow = _unitOfWord.Flower.Find(flowerId);
            flow.SalePrice = flow.Price - (flow.Price * saleOff / 100);
            _unitOfWord.Flower.Update(flow);
            _unitOfWord.SaveChange();
            return View();
        }

        public JsonResult OnGetGetProducts(Guid? categoryId)
        {
            var products = _unitOfWord.Flower.GetFlowerByCategory(categoryId);
            IEnumerable<SelectListItem> result = products.Select(u => new SelectListItem
            {
                Text = u.FlowerName,
                Value = u.Id.ToString(),
            }
            );
            return new JsonResult(result);
        }
    }
}
