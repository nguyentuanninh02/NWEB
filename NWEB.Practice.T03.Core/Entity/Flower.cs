﻿using NWEB.Practice.T03.Core.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NWEB.Practice.T03.Core.Entity
{
    public class Flower
    {
        public Guid Id { get; set; }
        [Required(ErrorMessage = "Category is required")]
        public Guid CategoryId { get; set; }
        public Category? Category { get; set; }
        [Required(ErrorMessage = "Flower Name is required")]
        [StringLength(255, ErrorMessage = "Max length of Flower Name Name is 255")]
        public string FlowerName { get; set; }
        public string? Description {  get; set; }
        public Color? Color { get; set; }
        public string? Image {  get; set; }
        [Required(ErrorMessage = "Flower Price is required")]
        public decimal Price {  get; set; }
        public decimal? SalePrice { get; set; }
        [Required(ErrorMessage = "Flower Store Date is required")]
        public DateTime StoreDate { get; set; }
        [Required(ErrorMessage = "Flower Store Inventory is required")]
        public int StoreInventory {  get; set; }
    }
}
