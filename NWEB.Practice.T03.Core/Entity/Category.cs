﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NWEB.Practice.T03.Core.Entity
{
    public class Category
    {
        public Guid Id { get; set; }
        [Required(ErrorMessage ="Category is require")]
        [StringLength(100, ErrorMessage = "Max length of Category Name is 100")]
        public string CategoryName { get; set; }
        [Required(ErrorMessage = "Order is require")]
        public int Order { get; set; } = 1;
        public string? Notes { get; set; }
    }
}
