﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using NWEB.Practice.T03.Core.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NWEB.Practice.T03.Core.Config
{
    public class FlowerConfig : IEntityTypeConfiguration<Flower>
    {
        public void Configure(EntityTypeBuilder<Flower> builder)
        {
            builder.ToTable("Flower");
            builder.HasKey(x => x.Id);
            builder.Property(x=> x.Id).IsRequired();
            builder.Property(x=> x.Id).ValueGeneratedOnAdd();
        }
    }
}
