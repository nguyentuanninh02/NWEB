﻿using Azure.Core;
using Microsoft.AspNetCore.Identity;
using NWEB.Practice.T03.Core.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NWEB.Practice.T03.Core.DbInit
{
    public class DbInitializer : IDbInitializer
    {
        private readonly UserManager<IdentityUser> _userManager;
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly ApplicationDbContext _db;

        public DbInitializer(
            UserManager<IdentityUser> userManager,
            RoleManager<IdentityRole> roleManager,
            ApplicationDbContext db)
        {
            _roleManager = roleManager;
            _userManager = userManager;
            _db = db;
        }

        public void Initialize()
        {
            if (!_roleManager.RoleExistsAsync(SD.ROLE_ADMIN).GetAwaiter().GetResult())
            {
                _roleManager.CreateAsync(new IdentityRole(SD.ROLE_ADMIN)).GetAwaiter().GetResult();
                _roleManager.CreateAsync(new IdentityRole(SD.ROLE_USER)).GetAwaiter().GetResult();

                _userManager.CreateAsync(new IdentityUser
                {
                    UserName = "admin@gmail.com",
                    Email = "admin@gmail.com",
                    PhoneNumber = "12356789",

                }, "Tuanninh30@").GetAwaiter().GetResult();

                IdentityUser user = _db.IdentityUser.FirstOrDefault(u => u.Email == "admin@gmail.com");

                _userManager.AddToRoleAsync(user, SD.ROLE_ADMIN).GetAwaiter().GetResult();

                //student account
                _userManager.CreateAsync(new IdentityUser
                {
                    UserName = "user@gmail.com",
                    Email = "user@gmail.com",
                    PhoneNumber = "0338516217",

                }, "Tuanninh30@").GetAwaiter().GetResult();

                IdentityUser student1 = _db.IdentityUser.FirstOrDefault(u => u.Email == "user@gmail.com");

                _userManager.AddToRoleAsync(student1, SD.ROLE_USER).GetAwaiter().GetResult();
                //
                _userManager.CreateAsync(new IdentityUser
                {
                    UserName = "user1@gmail.com",
                    Email = "user1@gmail.com",
                    PhoneNumber = "0338516217",

                }, "Tuanninh30@").GetAwaiter().GetResult();

                IdentityUser student2 = _db.IdentityUser.FirstOrDefault(u => u.Email == "user1@gmail.com");

                _userManager.AddToRoleAsync(student2, SD.ROLE_USER).GetAwaiter().GetResult();
                //
                _userManager.CreateAsync(new IdentityUser
                {
                    UserName = "user2@gmail.com",
                    Email = "user2@gmail.com",
                    PhoneNumber = "0338516217",

                }, "Tuanninh30@").GetAwaiter().GetResult();

                IdentityUser student3 = _db.IdentityUser.FirstOrDefault(u => u.Email == "user2@gmail.com");

                _userManager.AddToRoleAsync(student3, SD.ROLE_USER).GetAwaiter().GetResult();
                //
                _userManager.CreateAsync(new IdentityUser
                {
                    UserName = "user3@gmail.com",
                    Email = "user3@gmail.com",
                    PhoneNumber = "0338516217",

                }, "Tuanninh30@").GetAwaiter().GetResult();

                IdentityUser student4 = _db.IdentityUser.FirstOrDefault(u => u.Email == "user3@gmail.com");

                _userManager.AddToRoleAsync(student4, SD.ROLE_USER).GetAwaiter().GetResult();

            }
            return;
        }
    }
}
