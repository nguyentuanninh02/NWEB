﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NWEB.Practice.T03.Core.Enums
{
    public enum Color
    {
        Red, Pink, Blue, Lavender, Orange, Purple
    }
}
