﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using NWEB.Practice.T03.Core.Config;
using NWEB.Practice.T03.Core.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Emit;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace NWEB.Practice.T03.Core.Context
{
    public class ApplicationDbContext: IdentityDbContext
    {
        public ApplicationDbContext()
        {
        }
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options) { }

        public DbSet<Category> Category { get; set; }
        public DbSet<Flower> Flower { get; set; }
        public DbSet<IdentityUser> IdentityUser { get; set; }
        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            builder.ApplyConfiguration(new CategoryConfig());
            builder.ApplyConfiguration(new FlowerConfig());

            builder.Entity<Category>().HasData(
                new Category { Id = new Guid("51de0b19-281d-4440-94ca-2b700780c922"), CategoryName = "Lavender", Order = 1 },
                new Category { Id = new Guid("9d921773-06a9-42ff-877c-65908874c496"), CategoryName = "Rose", Order = 2 },
                new Category { Id = new Guid("21085f82-a1eb-425c-be2d-388b17123562"), CategoryName = "Sunflower", Order = 3 },
                new Category { Id = new Guid("58a5a08d-5604-4f2d-a646-027b828ab934"), CategoryName = "Lotus", Order = 4 },
                new Category { Id = new Guid("4f140794-1a29-4faf-9d8b-2b8baa264d1d"), CategoryName = "Hydrangeas", Order = 5 }
            );

            builder.Entity<Flower>().HasData(
                new Flower
                {
                    Id = new Guid("e89d2eb2-0090-48c3-9ac6-f2d02176f419"),
                    CategoryId= Guid.Parse("51de0b19-281d-4440-94ca-2b700780c922"),
                    FlowerName= "Birthday Flower1",
                    Price= 32,
                    StoreDate= new DateTime(2023, 09, 10),
                    StoreInventory= 1
                },
                new Flower
                {
                    Id = new Guid("3d736de3-c6a1-4425-94b6-ad973e0436b2"),
                    CategoryId = Guid.Parse("4f140794-1a29-4faf-9d8b-2b8baa264d1d"),
                    FlowerName = "Birthday Flower2",
                    Price = 32,
                    StoreDate = new DateTime(2023, 09, 10),
                    StoreInventory = 1
                },
                new Flower
                {
                    Id = new Guid("152109cd-c8c0-4a1e-8747-c8dd3731d417"),
                    CategoryId = Guid.Parse("4f140794-1a29-4faf-9d8b-2b8baa264d1d"),
                    FlowerName = "Birthday Flower3",
                    Price = 32,
                    StoreDate = new DateTime(2023, 09, 10),
                    StoreInventory = 1
                },
                new Flower
                {
                    Id = new Guid("868d8eaf-f0f1-44ca-8a93-8034adb74359"),
                    CategoryId = Guid.Parse("51de0b19-281d-4440-94ca-2b700780c922"),
                    FlowerName = "Birthday Flower4",
                    Price = 32,
                    StoreDate = new DateTime(2023, 09, 10),
                    StoreInventory = 1
                },
                new Flower
                {
                    Id = new Guid("36bdab85-862c-43fa-899f-49a6c14e43ce"),
                    CategoryId = Guid.Parse("51de0b19-281d-4440-94ca-2b700780c922"),
                    FlowerName = "Birthday Flower5",
                    Price = 32,
                    StoreDate = new DateTime(2023, 09, 10),
                    StoreInventory = 1
                }
                );
        }

    }
}
