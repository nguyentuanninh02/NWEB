﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NWEB.Practice.T03.DataAccessLayer.IRepository
{
    public interface IUnitOfWork
    {
        ICategoryRepository Category { get; }
        IFlowerRepository Flower { get; }

        int SaveChange();
    }
}
