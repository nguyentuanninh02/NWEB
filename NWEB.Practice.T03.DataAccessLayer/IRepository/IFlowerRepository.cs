﻿using NWEB.Practice.T03.Core.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Net.Mime.MediaTypeNames;

namespace NWEB.Practice.T03.DataAccessLayer.IRepository
{
    public interface IFlowerRepository: IGenericRepository<Flower>
    {
        IEnumerable<Flower> GetAllWithCategory();
        List<Flower> GetFlowerByCategory(Guid? id);
    }

}
