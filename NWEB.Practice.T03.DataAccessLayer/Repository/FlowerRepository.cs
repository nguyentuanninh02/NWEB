﻿using Microsoft.EntityFrameworkCore;
using NWEB.Practice.T03.Core.Context;
using NWEB.Practice.T03.Core.Entity;
using NWEB.Practice.T03.DataAccessLayer.IRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NWEB.Practice.T03.DataAccessLayer.Repository
{
    public class FlowerRepository : GenericRepository<Flower>, IFlowerRepository
    {
        public FlowerRepository(ApplicationDbContext dbContext) : base(dbContext)
        {
        }

        public IEnumerable<Flower> GetAllWithCategory()
        {
            return dbContext.Flower.Include(f => f.Category).ToList();
        }

        public List<Flower> GetFlowerByCategory(Guid? id)
        {
            var products = dbContext.Flower
            .Where(p => p.CategoryId == id)
            .ToList();

            return products;
        }
    }
}
