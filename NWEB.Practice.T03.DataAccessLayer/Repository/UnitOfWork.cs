﻿using NWEB.Practice.T03.Core.Context;
using NWEB.Practice.T03.DataAccessLayer.IRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NWEB.Practice.T03.DataAccessLayer.Repository
{
    public class UnitOfWork: IUnitOfWork
    {
        public ICategoryRepository Category { get; set; }
        public IFlowerRepository Flower { get; set; }

        public readonly ApplicationDbContext _db;

        public UnitOfWork(ApplicationDbContext db)
        {
            _db= db;
            Category = new CategoryRepository(db);
            Flower = new FlowerRepository(db);
        }
        public int SaveChange()
        {
            return _db.SaveChanges();
        }
    }
}
